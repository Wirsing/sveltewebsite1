#!/bin/bash

# Schritt 1: Lokales Git-Repository erstellen
echo "Schritt 1: Lokales Git-Repository erstellen"
read -p "Gib den Namen des Ordners für das Repository ein: " repo_name


git init

# Schritt 2: Commit-Nachricht abfragen
echo "Schritt 2: Commit-Nachricht abfragen"
read -p "Gib die Commit-Nachricht ein: " commit_message

# Schritt 3: Codeberg-Benutzername abfragen
echo "Schritt 3: Codeberg-Benutzername abfragen"
read -p "Gib deinen Codeberg-Benutzernamen ein: " codeberg_username

# Schritt 4: Remote-Repository verbinden (Codeberg)
echo "Schritt 4: Remote-Repository verbinden (Codeberg)"
git remote add origin "https://codeberg.org/$codeberg_username/$repo_name.git"
git add .
git commit -m "$commit_message"

git push -u origin main

echo "Das Repository wurde erstellt und mit Codeberg verbunden."
